import React, { Component } from 'react';
import {Form, Icon, Input, Button, Checkbox} from 'antd';
import { BrowserRouter as Router, Route, Link,Switch} from "react-router-dom";
import 'antd/dist/antd.css';
import './Login.css';
import Indexs from './Indexs';

class Login extends Component {
      constructor(props) {
        super(props);
        this.handleSubmit=this.handleSubmit.bind(this)
    } 
              
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
    
      <Form onSubmit={this.handleSubmit} className="login-form" >
      <h1 style={{color:'#FFF'}}>React-admin-template</h1>
        <Form.Item>
          {getFieldDecorator('userName', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input prefix={<Icon type="user" style={{ color: '#889AA4' }} />} placeholder="Username" />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input.Password prefix={<Icon type="lock" style={{ color: '#889AA4' }} />} type="password" placeholder="Password" />
          )}
        </Form.Item>
        <Form.Item>
          
          <Button type="primary" htmlType="submit" className="login-form-button"><Link to="/Indexs">
            Log in</Link>
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);
export default  WrappedNormalLoginForm;