import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './indexs.css';
import Routerz from './Routerz';
import { BrowserRouter as Router, Route, Link,Switch} from "react-router-dom";
import { Layout, Menu, Icon , Breadcrumb, Dropdown } from 'antd';
import Form from './Form';
import Analysis from './Analysis';
import Menus from './Menus';
import Home from './Home';
import Work from './Work';
import Login from './Login';


const SubMenu = Menu.SubMenu;
const { Header, Sider, Content } = Layout;

    const menu = (
       <Menu>
           <Menu.Item key="0">
             <Link to="/Indexs">Home</Link>
           </Menu.Item>
           <Menu.Item key="1">
             <Link to="/">LogOut</Link>
           </Menu.Item>
        </Menu>
    );

    class Indexs extends React.Component {
       constructor(props) {
          super(props);
          this.state = {collapsed: false,srt:""}
          this.ch1=this.ch1.bind(this)
          this.ch2=this.ch2.bind(this)
          this.ch3=this.ch3.bind(this)
          this.ch4=this.ch4.bind(this)
          this.ch5=this.ch5.bind(this)
       }

       toggle = () => {this.setState({collapsed: !this.state.collapsed,});}

       ch1 = () => {this.setState({str : "Example/Tables",})};
       ch2 = () => {this.setState({str : "Example/Three",})};
       ch3 = () => {this.setState({str : "Form",})};
       ch4 = () => {this.setState({str : "Nested/Menu",})};
       ch5= () => {this.setState({str : "Donate",})};

    render() {
       return (
           <Layout>
               <Sider 
                    trigger={null} 
                    collapsible 
                    collapsed={this.state.collapsed} 
                    onCollapse={this.onCollapse}>

         <div className="logo" />
               <Menu theme="dark" mode="inline" defaultOpenKeys={['sub1']}>
                   <SubMenu key="sub1" title={<span><Icon type="sync" /><span>Example</span></span>}>

                    <Menu.Item key="1" onClick={this.ch1}>
                   <Link to='/Indexs/Analysis'>
                    <Icon type="appstore" theme="filled" />
                    <span>Table</span></Link>
               </Menu.Item>
               <Menu.Item key="2" onClick={this.ch2}>
                    <Link to="/Indexs/Work">
                    <Icon type="appstore" theme="filled" />
                    <span>工作台</span></Link>
               </Menu.Item>

                   </SubMenu>
          
              <Menu.Item key="sub2" onClick={this.ch3}>
              <Link to="/Indexs/Form">
                     <Icon type="form" />
                     <span>Form</span></Link>
              </Menu.Item>

              <SubMenu key="sub3" title={<span><Icon type="align-left" /><span>Nested</span></span>}>
              <SubMenu key="sub2" title={<span><span>Menu1</span></span>}>
                 <Menu.Item key="9"><Link to='/Indexs/Menus'>Menu1-1</Link></Menu.Item>
              <SubMenu key="sub6" title={<span><span>Menu1-2</span></span>}>
                 <Menu.Item key="7"><Link to='/Indexs/Menus'>Menu1-2-1</Link></Menu.Item>
                 <Menu.Item key="3"><Link to='/Indexs/Menus'>Menu1-2-2</Link></Menu.Item>
              </SubMenu>
                 <Menu.Item key="10"><Link to='/Indexs/Menus'>Menu1-3</Link></Menu.Item>
              </SubMenu>
                 <Menu.Item key="5" onClick={this.ch4}><Link to='/Indexs/Menus'>Menu2</Link></Menu.Item>
              </SubMenu>

                 <Menu.Item key="sub4" onClick={this.ch5}>
                 <a href="https://panjiachen.github.io/vue-element-admin-site/#/">
                    <Icon type="select" />
                    <span>Exterate Link</span></a>
                 </Menu.Item>

                 <Menu.Item key="sub7" onClick={this.ch5}>
                 <a href="https://panjiachen.gitee.io/vue-element-admin-site/zh/donate/">
                    <Icon type="fire" />
                    <span>Donate</span></a>
                 </Menu.Item>
                 
                 
                 
            </Menu>
        </Sider>

    <Layout className="top">
      <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              style={{marginLeft:'20px'}}
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
          <Breadcrumb separator=">" style={{display: 'inline-block',marginLeft:'20px'}}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item href="">{this.state.str}</Breadcrumb.Item>
          </Breadcrumb>

        <Dropdown overlay={menu} trigger={['click']} className="topright">
          <div className="ant-dropdown-link" >
          <img src="https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80" className="user-avatar" />
       <Icon type="caret-down" />
          </div>
       </Dropdown>
      </Header>

          <Content style={{ margin: '20px 16px'}}>
            <a href="" className="contlefta"><span className="contleftspan">ZingGrid</span>makes powerful, feature-rich JS data grids easy to build, with minimal setup</a>
            <a href="" className="contrighta"><i>ethical</i> ad by CodeFund </a>
          </Content>
          <Content style={{paddingLeft:"7%", minHeight: 280}}>
          <Switch>
           <Route exact path="/Indexs/" component={Home} />
           <Route path="/Indexs/Form" component={Form} />
           <Route path="/Indexs/Analysis" component={Analysis} />
           <Route path="/Indexs/Work" component={Work} />
           <Route path="/Indexs/Menus" component={Menus} />
           </Switch> 
          </Content>
          
          
        </Layout>
      </Layout>
    );
  }
}
export default Indexs;