import React from 'react';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';


class Test extends React.Component {
  componentDidMount() {
    // 初始化
    var myChart = echarts.init(document.getElementById('main'));
    // 绘制图表
    myChart.setOption({
        title: { text: '某地区蒸发量和降水量' },
        tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'line'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['1', 'e', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun','1', 'e', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    yAxis: {
        type: 'value'
    },
    series: [{
        data: [27, 40, 901, 934, 1290, 1330, 1320, 901, 934, 1290, 1330, 1320],
        type: 'line',
        areaStyle: {}
    }]


    });
}
render() {
    return (
        <div id="main" style={{ width: '25', height: 200 }}></div>
    );
}
}

export default Test;