import React, { Component } from 'react';
import { Row, Col,Icon,Tooltip,Progress,Tabs } from 'antd';
import './Analysis.css';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';

const TabPane = Tabs.TabPane;
class Analysis extends Component {
  constructor(props) {
    super(props);

  }
  charts1(){
      // 初始化
     var myChart = echarts.init(document.getElementById('main'));
     // 绘制图表
     myChart.setOption({
         tooltip : {
         trigger: 'axis',
        
     },
      grid: {show:'true',borderWidth:'0',height:"64%",width:"70%",y:"20%",x:"12%"},
     xAxis: {
         type: 'category',
         boundaryGap: false,
         data: [0, 40, 20, 30, 40, 30, 50, 40, 30, 15, 20, 40, 20, 30,0],
     },
     yAxis: {
            axisLabel : {
           formatter: function(){
                 return "";
           }
       }
     },
     series: [{
         data: [0, 40, 20, 30, 40, 30, 50, 40, 30, 15, 20, 40, 20, 30,0],
         type: 'line',
         areaStyle: {}
     }]
     }); 
  }
  
   charts2(){
      // 初始化
     var myChart = echarts.init(document.getElementById('main1'));
     // 绘制图表
     myChart.setOption({
         color: ['#3398DB'],
         tooltip : {
         trigger: 'axis',
        
     },
      grid: {show:'true',borderWidth:'0',height:"64%",width:"70%",y:"20%",x:"12%"},
     xAxis: {
         type: 'category',
         boundaryGap: false,
         data: [0, 40, 20, 30, 40, 30, 50, 40, 30, 15, 20, 40, 20, 30,0],
         axisTick: {
                alignWithLabel: true},
     },
     yAxis: {
            axisLabel : {
           formatter: function(){
                 return "";
           }
       }
     
     },
     series: [{
         data: [0, 40, 20, 30, 40, 30, 50, 40, 30, 15, 20, 40, 20, 30,0],
         type: 'bar',
         barWidth: '30%',
         
     }]
       
       
     }); 
  }
  
  charts3(){
      // 初始化
     var myChart = echarts.init(document.getElementById('main2'));
     // 绘制图表
     myChart.setOption({
         color: ['#3398DB'],
         title: { text: '访问量趋势' },
         tooltip : {
         trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
     },
      grid: {show:'true',left:'3%',borderWidth:'0',height:"70%",width:"80%",y:"20%",x:"2%"},
     xAxis: {
         type: 'category',
        
         data: ['1月', '2月', '3月', '4月','5月','6月', '7月','8月', '9月', '10月','11月', '12月'],
         axisTick: {
                alignWithLabel: true},
     },
     yAxis: {
            type : 'value',
     },
     series: [{
         data: [851,377,957, 1128, 424,1093,871, 499, 854,1038, 1044, 498],
         type: 'bar',
          barWidth: '40%',
         
     }]
       
       
     }); 
  }
  
 
  componentDidMount() {
     this.charts1();
     this.charts2();
     this.charts3();
   
  }
  callback(key) {
  console.log(key);
}
  
   render() {
    return (
        <div className="analy">
         <Row>
      <Col sm={5} className="ana-col">
         <div className="ana">
         <div className="sumbuy"><span>总销售额</span>
          <Tooltip placement="top" title="指标说明" arrowPointAtCenter>
           <Icon className="ana-icon" type="info-circle" />
          </Tooltip>
         </div>
         <p className="money">¥ 126,560</p>
         <p className="percent">周同比 12%<Icon className="per-up" type="caret-up" /></p>
         <p className="percent">日同比 11%<Icon className="per-down" type="caret-down" /></p>
         <p className="buymoney">日销售额  ¥12,423</p>
         </div>
         </Col>
      <Col sm={5} className="ana-col">
          <div className="ana">
         <div className="sumbuy"><span>访问量</span>
          <Tooltip placement="top" title="指标说明" arrowPointAtCenter>
           <Icon className="ana-icon" type="info-circle" />
          </Tooltip>
         </div>
         <p className="money">8,846</p>
         <div id="main"></div>
         <p className="buymoney">日访问量 1,234</p>
         </div>
         </Col>
       <Col sm={5} className="ana-col">
          <div className="ana">
         <div className="sumbuy"><span>支付笔数</span>
          <Tooltip placement="top" title="指标说明" arrowPointAtCenter>
           <Icon className="ana-icon" type="info-circle" />
          </Tooltip>
         </div>
         <p className="money">6,560</p>
         <div id="main1"></div>
         <p className="buymoney">转化率 60%</p>
         </div>
         </Col>
     <Col sm={6} className="ana-col">
         <div className="ana">
         <div className="sumbuy"><span>运营活动效果</span>
          <Tooltip placement="top" title="指标说明" arrowPointAtCenter>
           <Icon className="ana-icon" type="info-circle" />
          </Tooltip>
         </div>
         <p className="money">78%</p>
         <div className="progress"><Tooltip placement="topRight" title="目标值：80%" arrowPointAtCenter>
         <Progress percent={78} status="active" />
         </Tooltip>
         </div>
         <div className="buymoneys">
         <span className="percent">周同比 12%<Icon className="per-up" type="caret-up" /></span>
         <span className="percent">日同比 11%<Icon className="per-down" type="caret-down" /></span>
         </div>
         </div>
     </Col>
         </Row>
        
          <Tabs className="tabs" defaultActiveKey="1" onChange={this.callback.bind(this)}>
    <TabPane tab="访问量" key="1">
     <Row>
         <Col sm={14}><div id="main2"></div> </Col>
         <Col sm={10}>
         <div className="tabs-col">
         <h3>门店访问量排名</h3>
         <li className="tab-li">
         <Icon className="tab-icon" type="smile" theme="twoTone" twoToneColor="#52c41a"/>
         <span className="tab-span1">工专路 0 号店</span>
         <span className="tab-span2">323,234</span></li>
         <li className="tab-li">
         <Icon className="tab-icon" type="smile" theme="twoTone" twoToneColor="#52c41a"/>
         <span className="tab-span1">工专路 0 号店</span>
         <span className="tab-span2">323,234</span></li>
         <li className="tab-li">
         <Icon className="tab-icon" type="smile" theme="twoTone" twoToneColor="#52c41a"/>
         <span className="tab-span1">工专路 0 号店</span>
         <span className="tab-span2">323,234</span></li>
         <li className="tab-li">
         <Icon className="tab-icon" type="meh" theme="twoTone" twoToneColor="#FF0000"/>
         <span className="tab-span1">工专路 0 号店</span>
         <span className="tab-span2">323,234</span></li>
         <li className="tab-li">
         <Icon className="tab-icon" type="meh" theme="twoTone" twoToneColor="#FF0000"/>
         <span className="tab-span1">工专路 0 号店</span>
         <span className="tab-span2">323,234</span></li>
         </div>
         </Col>
         </Row>
    </TabPane>
    
    <TabPane tab="总销售额" key="2">
    <div id="main3"></div>
    </TabPane>
   
    
    
  </Tabs>
         
         
         
         
        
         
         
         
         </div>
    );
    }}
export default Analysis;