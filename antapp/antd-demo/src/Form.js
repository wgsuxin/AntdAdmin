import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import {
  Form, Select, InputNumber, Switch, Radio,
  Slider, Button, Upload, Icon, Rate, Checkbox,
  Row, Col, Input ,DatePicker, TimePicker 
} from 'antd';

const { Option } = Select;
const { MonthPicker, RangePicker } = DatePicker;
const { TextArea } = Input;
class Forme extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
      
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 18 },
    };
    const config = {
      rules: [{ type: 'object', required: true, message: 'Please select time!' }],
    };
    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>

         <Form.Item label="Activity name">
          {getFieldDecorator('nickname', {
            rules: [{ required: true, message: 'Please input your nickname!', whitespace: true }],
          })(
            <Input />
          )}
        </Form.Item>

         <Form.Item label="Select">
          {getFieldDecorator('select', {
            rules: [
              { required: true, message: 'Please select your country!' },
            ],
          })(
            <Select placeholder="Please select a country">
              <Option value="china">China</Option>
              <Option value="usa">U.S.A</Option>
            </Select>
          )}
        </Form.Item>

         <Form.Item label="DatePicker">
        <Row>
        <Col span={8}>
          {getFieldDecorator('date-picker', config)(
            <DatePicker />
          )}</Col>
          <Col span={8}>
          {getFieldDecorator('time-picker', config)(
            <TimePicker />
          )}
          </Col>
          </Row>
        </Form.Item>

        <Form.Item label="Switch">
          {getFieldDecorator('switch', { valuePropName: 'checked' })(
            <Switch />
          )}
        </Form.Item>

       <Form.Item label="Activity type">
         {getFieldDecorator("checkbox-group", {
           initialValue: ["", ""],
         })(
           <Checkbox.Group style={{ width: "100%" }}>
             <Row>
               <Col sm={6}><Checkbox value="A">Online activities</Checkbox></Col>
               <Col sm={6}><Checkbox  value="B">Promotion activities</Checkbox></Col>
               <Col sm={6}><Checkbox value="C">Offline activities</Checkbox></Col>
               <Col sm={6}><Checkbox value="D">Simple brand exposure</Checkbox></Col>
             </Row>
           </Checkbox.Group>
         )}
       </Form.Item>

        <Form.Item label="Resources">
          {getFieldDecorator('radio-group')(
            <Radio.Group>
              <Radio value="a">Sponsor</Radio>
              <Radio value="b">Venue 2</Radio>
            </Radio.Group>
          )}
        </Form.Item>

       <Form.Item label="TextArea">
         {getFieldDecorator('radio-group')(
         <TextArea placeholder="Autosize height with minimum and maximum number of lines" autosize={{ minRows: 2, maxRows: 6 }} />
         )}
       </Form.Item>

        <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
          <Button type="primary" htmlType="submit">Submit</Button>
        </Form.Item>
      </Form>
    );
  }
}

const WrappedDemo = Form.create({ name: 'validate_other' })(Forme);
export default WrappedDemo;